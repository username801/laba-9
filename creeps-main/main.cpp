#include "sort.h"
#include "protocol.h"
#include "file-read.h"
#include "const.h"
#include "filter.h"
#include "process.h"
#include <iomanip>
#include <iostream>
#include <cstring>
using namespace std;
void output(protocol* protocols) {
    cout << "Время начала: ";
    cout << setw(2) << setfill('0') << protocols->start.hour << ':';
    cout << setw(2) << setfill('0') << protocols->start.minute << ':';
    cout << setw(2) << setfill('0') << protocols->start.second << '\n';
    cout << "Время конца: ";
    cout << setw(2) << setfill('0') << protocols->finish.hour << ':';
    cout << setw(2) << setfill('0') << protocols->finish.minute << ':';
    cout << setw(2) << setfill('0') << protocols->finish.second << '\n';
    cout << "Байт получено: ";
    cout << protocols->byteIN << '\n';
    cout << "Байт отправлено: ";
    cout << protocols->byteOUT << '\n';
    cout << "Программа: ";
    cout << protocols->name << '\n';
    cout << '\n';
}
int main(int argc, char* argv[])
{
    setlocale(LC_ALL, "Russian");
    cout << "лаб. работа #9. GIT\n";
    cout << "вар. 5: протокол работы в интернете\n";
    cout << "автор: Егор Никитин\n\n";
    protocol* protocols[MAX_FILE_ROWS_COUNT];
    int size;
    try {
        read("data.txt", protocols, size);
        cout << "*****Протокол работы в Интернете*****\n\n";
        for (int i = 0; i < size; i++) {
            output(protocols[i]);
        }
        bool (*check_function)(protocol*) = NULL;
        cout << "\nВведите функцию фильтрации:\n";
        cout << "1) вывести протоколы с программой Skype\n";
        cout << "2) вывести протоколы после 8:00:00 \n";
        cout << "3) вывести суммарное время использования сети Интернет определенной программой\n";
        int filt;
        cin >> filt;
        switch (filt)
        {
        case 1:
            check_function = check_protocol_by_name;
            cout << "*****Протоколы с программой Skype*****\n\n";
            break;
        case 2:
            check_function = check_protocol_by_time;
            cout << "*****Протоколы после 8:00:00*****\n\n";
            break;
        case 3:
        {
            int seconds = process(protocols, size);
            cout << "Суммарное время (в секундах) - " << seconds << "\n\n";
            break;
        }
        default:
            throw "Error";
        }
        if (check_function)
        {
            int new_size;
            protocol** filtered = filter(protocols, size, check_function, new_size);
            for (int i = 0; i < new_size; i++)
            {
                output(filtered[i]);
            }
            delete[] filtered;
        }
        cout << "\nВведите сортировку:\n";
        cout << "1) вывести протоколы по убыванию времени использования сети Интернет\n";
        cout << "(разницы между временем конца и начала использования)\n";
        cout << "2) вывести протоколы по возрастанию названия программы, а в рамках одной программы\n";
        cout << "по убыванию суммарного количества переданных и полученных байт \n";
        int sort;
        cin >> sort;
        switch (sort) {
        case 1: {
            cout << "\nВведите тип сортировки:\n";
            cout << "1) сортировка вставками\n";
            cout << "2) быстрая сортировка  \n";
            int sorttype;
            cin >> sorttype;
            switch (sorttype) {
            case 1: {
                timeDown_insertion(protocols, size); //checked, works normally
                break;
            }
            case 2: {
                timeDown_quick(protocols, size);
                break;
            }
            default: {
                cout << "\nError\n";
                break;
            }
            }
            break;
        }
        case 2: {
            cout << "\nВведите тип сортировки:\n";
            cout << "1) сортировка вставками\n";
            cout << "2) быстрая сортировка  \n";
            int sorttype;
            cin >> sorttype;
            switch (sorttype) {
            case 1: {
                nameUp_insertion(protocols, size);
                byteDown_insertion(protocols, size);
                break;
            }
            case 2: {
                nameUp_quick(protocols, size);
                byteDown_quick(protocols, size);
                break;
            }
            default: {
                cout << "\nError\n";
                break;
            }
            }
            break;
        }
        default: {
            cout << "\nError\n";
            break;
        }
        }
        for (int i = 0; i < size; i++) {
            output(protocols[i]);
        }
        for (int i = 0; i < size; i++) {
            delete protocols[i];
        }
    }
    catch (const char* error) {
        cout << error << '\n';
    }
    return 0;
}