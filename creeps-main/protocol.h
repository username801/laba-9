#ifndef PROTOCOL_H
#define PROTOCOL_H

#include "const.h"

struct Time
{
    int hour;
    int minute;
    int second;
};

struct protocol
{
    Time start;
    Time finish;
    int byteIN;
    int byteOUT;
    char name[MAX_STRING_SIZE];
};

#endif