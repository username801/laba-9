#include "filter.h"
#include <cstring>
#include <iostream>
protocol** filter(protocol* array[], int size, bool (*check)(protocol* element), int& result_size) {
	protocol** result = new protocol * [size];
	result_size = 0;
	for (int i = 0; i < size; i++) {
		if (check(array[i])) {
			result[result_size++] = array[i];
		}
	}
	return result;
}
bool check_protocol_by_name(protocol* element) {
	return strcmp(element->name, "Skype") == 0;
}
bool check_protocol_by_time(protocol* element) {
	return element->start.hour >= 8 && element->start.hour <= 23;
}