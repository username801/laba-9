#include "sort.h"
#include "time.h"
#include <cstring>
#include <iostream>
using namespace std;
void quicksortTIME(protocol* arr[], int left, int right, int timeTotal[])
{
	if (left >= right)
		return;
	int i, j;
	bool m;
	for (i = left, j = right, m = true; i < j; m ? j-- : i++)
		if (timeTotal[i] < timeTotal[j]) {
			swap(arr[i], arr[j]);
			swap(timeTotal[i], timeTotal[j]);
			m = !m;
		}
	quicksortTIME(arr, left, i - 1, timeTotal);
	quicksortTIME(arr, i + 1, right, timeTotal);
	return;
}

void quicksortNAME(protocol* arr[], int left, int right)
{
	if (left >= right)
		return;
	int i, j;
	bool m;
	for (i = left, j = right, m = true; i < j; m ? j-- : i++)
		if (strcmp(arr[i]->name, arr[j]->name) > 0) {
			swap(arr[i], arr[j]);
			m = !m;
		}
	quicksortNAME(arr, left, i - 1);
	quicksortNAME(arr, i + 1, right);
	return;
}

void quicksortBYTE(protocol* arr[], int left, int right, int byteTotal[])
{
	if (left >= right)
		return;
	int i, j;
	bool m;
	for (i = left, j = right, m = true; i < j; m ? j-- : i++)
		if (byteTotal[i] < byteTotal[j] && strcmp(arr[i]->name, arr[j]->name) == 0) {
			swap(arr[i], arr[j]);
			swap(byteTotal[i], byteTotal[j]);
			m = !m;
		}
	quicksortBYTE(arr, left, i - 1, byteTotal);
	quicksortBYTE(arr, i + 1, right, byteTotal);
	return;
}

void timeDown_insertion(protocol* protocol[], int size) {
	int* timeTotal = new int[size];
	for (int i = 0; i < size; i++)
		timeTotal[i] = timeTotalToSeconds(protocol[i]);
	int mini = 0;
	for (int i = 0; i < size - 1; i++) {
		mini = i;
		for (int j = i + 1; j < size; j++)
			if (timeTotal[j] > timeTotal[mini])
				mini = j;
		swap(protocol[i], protocol[mini]);
		swap(timeTotal[i], timeTotal[mini]);
	}
}

void timeDown_quick(protocol* protocol[], int size) {
	int* timeTotal = new int[size];
	for (int i = 0; i < size; i++)
		timeTotal[i] = timeTotalToSeconds(protocol[i]);
	quicksortTIME(protocol, 0, size - 1, timeTotal);
}

void nameUp_insertion(protocol* protocol[], int size) {
	int mini = 0;
	for (int i = 0; i < size - 1; i++) {
		mini = i;
		for (int j = i + 1; j < size; j++)
			if (strcmp(protocol[j]->name, protocol[mini]->name) < 0)
				mini = j;
		swap(protocol[i], protocol[mini]);
	}
}

void byteDown_insertion(protocol* protocol[], int size) {
	int* byteTotal = new int[size];
	for (int i = 0; i < size; i++)byteTotal[i] = protocol[i]->byteIN + protocol[i]->byteOUT;
	int mini = 0;
	for (int i = 0; i < size - 1; i++) {
		mini = i;
		for (int j = i + 1; j < size; j++)
			if ((byteTotal[j] > byteTotal[mini]) && strcmp(protocol[j]->name, protocol[mini]->name) == 0)
				mini = j;
		swap(protocol[i], protocol[mini]);
	}
}

void nameUp_quick(protocol* protocol[], int size) {
	quicksortNAME(protocol, 0, size - 1);
}

void byteDown_quick(protocol* protocol[], int size) {
	int* byteTotal = new int[size];
	for (int i = 0; i < size; i++)byteTotal[i] = protocol[i]->byteIN + protocol[i]->byteOUT;
	quicksortBYTE(protocol, 0, size - 1, byteTotal);
}

