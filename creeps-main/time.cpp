#include "time.h"
#include "protocol.h"
int timeTotalToSeconds(protocol* protocol) {
    int timeStart;
    int timeFinish;
    int timeTotal;
    timeStart = protocol->start.hour * 60 * 60 + protocol->start.minute * 60 + protocol->start.second;
    timeFinish = protocol->finish.hour * 60 * 60 + protocol->finish.minute * 60 + protocol->finish.second;
    if (timeStart > timeFinish) timeTotal = 86400 - timeStart + timeFinish;
    else if (timeStart == timeFinish) timeTotal = 0;
    else timeTotal = timeFinish - timeStart;
    return timeTotal;
}