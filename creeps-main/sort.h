#ifndef SORT_H
#define SORT_H
#include "protocol.h"
void timeDown_insertion(protocol* protocol[], int size);
void timeDown_quick(protocol* protocol[], int size);
void nameUp_insertion(protocol* protocol[], int size);
void byteDown_insertion(protocol* protocol[], int size);
void nameUp_quick(protocol* protocol[], int size);
void byteDown_quick(protocol* protocol[], int size);
#endif

