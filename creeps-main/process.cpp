#include "process.h"
#include "protocol.h"
#include "time.h"
#include <cstring>
#include <iostream>
using namespace std;
int process(protocol* protocol[], int size)
{
	int sec = 0;
	char progName[MAX_STRING_SIZE];
	cout << "\nВведите название программы: \n";
	cin >> progName;
	for (int i = 0; i < size; i++) {
		if (strcmp(protocol[i]->name, progName) == 0)
			sec += timeTotalToSeconds(protocol[i]);
	}
	return sec;
}
